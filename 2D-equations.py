import math

from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from tqdm import tqdm

from base_equations import from_rimans_invariants, LinearSolver, c_0

time_seconds = 0.1  # Total time in seconds
dt = 0.004  # Time step in seconds
nt = int(time_seconds / dt)  # Amount of time steps
dx = 0.01
dy = 0.01

u0 = 1.0  # initial u value
po = 1.0
lx = 2.0  # Length along X in meters
ly = 1.0  # Length along Y in meters

nx = int(lx / dx)  # Number of nodes along X
ny = int(ly / dy)  # Number of nodes along Y


class d2_solver():
    def __init__(self):
        self.current_values = generate_initial_data()

        self.x_solver = LinearSolver(nx)
        self.y_solver = LinearSolver(ny)

        self.error = 0.0

    def test_solve(self):
        prev_err, curr_err = 1.0, 1.0
        for _ in tqdm(range(nt)):
            # stepX
            for j in range(ny):
                self.current_values[j, :, 0], self.current_values[j, :, 1] = \
                    self.x_solver.do_single_step(self.current_values[j, :, 0], self.current_values[j, :, 1])

            # step Y
            for i in range(nx):
                self.current_values[:, i, 0], self.current_values[:, i, 1] = \
                    self.y_solver.do_single_step(self.current_values[:, i, 0], self.current_values[:, i, 1])

            prev_err = curr_err
            curr_err = self.calculate_errors()[0]

            print(f'errors: {prev_err}, {curr_err}')
            print(f"log errors: {math.log2(curr_err / prev_err)}")

    def print_result(self):
        fig = plt.figure()
        ax = fig.gca(projection='3d')

        # Make data.
        x = np.arange(0, lx, dx)
        y = np.arange(0, ly, dy)
        # X, Y = np.meshgrid(x - [lx / 2] * len(x), y - [ly / 2] * len(y)) # for test function
        # Z = np.exp(-X**2 / 100 - Y**2 / 100) # for test function
        X, Y = np.meshgrid(x, y)
        print(np.array(self.current_values).shape)

        Z = np.array(self.current_values[:, :, 0])
        # Plot the surface.
        surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                               linewidth=0, antialiased=False)

        # Customize the z axis.
        # ax.set_zlim(-1.01, 1.01)
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

        # Add a color bar which maps values to colors.
        fig.colorbar(surf, shrink=0.5, aspect=5)

        plt.show()

    def calculate_errors(self):
        y_shift = int(c_0 * time_seconds / dx)
        x_shift = int(c_0 * time_seconds / dy)

        err1, err_inf = 0, 0

        initial_data = generate_initial_data()

        theoretical_data = np.zeros((ny, nx, 1))

        for j in range(ny):
            y_index = j + y_shift if j + y_shift < ny else j + y_shift - ny
            for i in range(nx):
                x_index = i + x_shift if i + x_shift < nx else i + x_shift - nx

                theoretical_data[j, i, 0] = initial_data[y_index, x_index, 0]

        resulting_data = self.current_values[:, :, 0] - theoretical_data[:, :, 0]

        for j in range(ny):
            for i in range(nx):
                err1 += abs(resulting_data[j, i]) * dx * dy
                if abs(resulting_data[j, i] > err_inf):
                    err_inf = resulting_data[j, i]

        return err1, err_inf


def generate_initial_data() -> np.ndarray:
    result = np.zeros((ny, nx, 3), dtype=float)
    for j in range(int(2 * ny / 5), int(ny / 5 * 4)):
        for i in range(int(nx / 5), int(nx / 5 * 4)):
            result[j][i] = [np.e ** (-(i * dx - lx / 2) ** 2 - (j * dy - ly / 2) ** 2), c_0, c_0]  #

    return result


if __name__ == "__main__":
    solver = d2_solver()
    solver.print_result()

    # print(solver.current_values[50, :])
    # %%

    solver.test_solve()
    solver.print_result()

    print(solver.calculate_errors())
    # %%

    # for i in range(5):
    #     solver = d2_solver()
    #     solver.print_result()
    #
    #
    #     solver.test_solve()
    #     solver.print_result()
    #
    #     print(solver.calculate_errors())
    #     dt = dt/2
    #     dx = dx/2
    #     dy = dy/2
