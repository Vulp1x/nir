import copy
from typing import Tuple, List

# N = 400
c_0 = 1.0
Z_0 = 1.0  # Z_0=rho_0∗c_0 7
k: float = 0.4
# h: float = 2.0 / N
# tau: float = k * h / c_0
# M = 0.25 / h / k


class W1:
    def __init__(self, N):
        self.d0 = 0.0
        self.d_1 = 0.0
        self.d1 = 0.0

        self.N = N

        self.current_values = [0.0 for _ in range(N)]
        self.next_time_value = [0.0 for _ in range(N)]

        # u_n-2, u_n-1, u_n u_n+1
        self.stencil_values = [0.0, 0.0, 0.0, 0.0]
        # self.stencil_d_values = [0.0, 0.0]
        # отступы для получения нужных значений
        self.stencil_right = [-2, -1, 0, 1]

    def do_single_step(self, current_values):
        self.current_values = current_values

        for i in range(self.N):
            self.fill_stencil_values(i)
            self.calculate_params()
            self.update_values(i)

        # self.copy_values_for_next_time()
        return self.next_time_value


        # return self
        # return copy.copy(self.next_time_value)

    def fill_stencil_values(self, index: int):
        for j in range(len(self.stencil_values)):
            new_index = index + self.stencil_right[j]
            if new_index > self.N - 1:
                self.stencil_values[j] = self.current_values[new_index - self.N]

            else:
                self.stencil_values[j] = self.current_values[new_index]

        return

    def calculate_params(self):
        # stencil_values: u_n-2, u_n-1, u_n u_n+1
        self.d0 = self.stencil_values[1] - self.stencil_values[2]
        self.d1 = self.stencil_values[2] - self.stencil_values[3]
        self.d_1 = self.stencil_values[0] - self.stencil_values[1]

    def update_values(self, index: int):
        self.next_time_value[index] = self.stencil_values[2] - 0.5 * c_0 * (self.d0 + self.d1) + \
                                      0.5 * c_0 * c_0 * (self.d0 - self.d1) - c_0 * (c_0 * c_0 - 1) / 6.0 * (
                                              self.d_1 - 2.0 * self.d0 + self.d1)


    def __str__(self):
        eps = 0.1
        string_to_return = ''
        string_to_return += 'W1:\n'
        for i in range(self.N):
            if eps > self.current_values[i] > -eps:
                string_to_return += '_'
            else:
                string_to_return += '|'

        return string_to_return


class W2():
    def __init__(self, N: int):
        self.d0 = 0.0
        self.d_1 = 0.0
        self.d1 = 0.0

        self.N = N

        self.current_values = [0.0 for _ in range(N)]
        self.next_time_value = [0.0 for _ in range(N)]

        # u_n-2, u_n-1, u_n u_n+1
        self.stencil_values = [0.0, 0.0, 0.0, 0.0]
        # self.stencil_d_values = [0.0, 0.0]
        # отступы для получения нужных значений
        self.stencil_left = [-2, -1, 0, 1]

    def do_single_step(self, current_values):
        self.current_values = current_values
        for i in range(self.N):
            self.fill_stencil_values(i)
            self.calculate_params()
            self.update_values(i)

        # self.copy_values_for_next_time()

        return self.next_time_value

        # return self
        # return copy.copy(self.next_time_value)

    def fill_stencil_values(self, index: int):
        for j in range(len(self.stencil_values)):
            new_index = index + self.stencil_left[j]
            if new_index > self.N - 1:
                self.stencil_values[j] = copy.copy(self.current_values[new_index - self.N])
            else:
                self.stencil_values[j] = copy.copy(self.current_values[new_index])

        return

    def calculate_params(self):
        # stencil_values: u_n-2, u_n-1, u_n u_n+1
        self.d0 = self.stencil_values[1] - self.stencil_values[2]
        self.d1 = self.stencil_values[2] - self.stencil_values[3]
        self.d_1 = self.stencil_values[0] - self.stencil_values[1]

    def update_values(self, index: int):
        self.next_time_value[index] = self.stencil_values[2] + 0.5 * c_0 * (self.d0 + self.d1) + \
                                      0.5 * c_0 * c_0 * (self.d0 - self.d1) + c_0 * (c_0 * c_0 - 1) / 6.0 * (
                                              self.d_1 - 2.0 * self.d0 + self.d1)

    def __str__(self):
        eps = 0.1
        string_to_return = 'W2:\n'
        for i in range(self.N):
            if self.current_values[i] < eps and self.current_values[i] > -eps:
                string_to_return += '_'
            else:
                string_to_return += '|'

        return string_to_return


def from_rimans_invariants(w1: List[float], w2: List[float]) -> Tuple[List[float], List[float]]:
    if len(w1) != len(w2):
        raise Exception(f'different shapes of riman invariants: {len(w1)} and {len(w2)}')

    p = [Z_0 * (-w1[i] + w2[i]) for i in range(len(w1))]
    u = [w1[i] + w2[i] for i in range(len(w1))]

    return (p, u)


def to_rimans_invariants(p: List, v: List) -> Tuple[List[float], List[float]]:
    if len(p) != len(v):
        raise Exception(f'different shapes for input params: {len(p)} and {len(v)}')

    w1 = [(-p[i] + Z_0 * v[i]) * 0.5 / Z_0 for i in range(len(p))]
    w2 = [(p[i] + Z_0 * v[i]) * 0.5 / Z_0 for i in range(len(p))]

    return (w1, w2)


class LinearSolver():
    def __init__(self, N):
        self.w1 = W1(N)
        self.w2 = W2(N)

    # returning pressure and velocity after this step
    def do_single_step(self, pressure, velocity):
        w1, w2 = to_rimans_invariants(pressure, velocity)
        next_time_w1 = self.w1.do_single_step(w1)
        next_time_w2 = self.w2.do_single_step(w2)

        return from_rimans_invariants(next_time_w1, next_time_w2)


if __name__ == '__main__':
    pass
    # w1 = W1(N)
    # w2 = W2(N)
    #
    # for step in range(round(M)):
    #     w1.do_single_step()
    #     w2.do_single_step()
    #
    #     # p, u = from_rimans_invariants(w1, w2)
    #     print(w1)
    #     print(w2.current_values)
    #
    # p, u = from_rimans_invariants(w1, w2)
    # plt.plot(np.linspace(0, 2.0, N), p)
    # plt.show()
